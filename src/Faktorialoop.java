import java.util.Scanner;

/*Pemrograman dibawah ini menggunakan paradigma pemrograman berorientasi objek */
public class Faktorial {
   /*Modifier private, variabel hanya dapat diakses di kelas yang sama */
   private int num;

   /*Constructor dengan parameter */
   public Faktorial(int num) {
      this.num = num;
   }

   /*Fungsi untuk menghitung faktorial*/
   public int hitung() {
      int factorial = 1;
      for (int i = 1; i <= num; i++) {
         factorial *= i;
      }
      return factorial;
   }

   public static void main(String[] args) {
      Scanner scanner = new Scanner(System.in);
      System.out.print("Enter a number: ");
      int num = scanner.nextInt();
      Faktorial calculator = new Faktorial(num);
      int result = calculator.hitung();
      System.out.println("The factorial of " + num + " is " + result);
   }
}

/*
Program yang sama yang ditulis dengan pendekatan pemrograman terstruktur.
Kelas ini akan error karena terdapat duplikasi kelas dengan nama yang sama.
Untuk menggunakan kelas dibawah pindahkan dalam file atau projek baru.
*/
public class Faktorial {
   public static void main(String[] args) {
      Scanner scanner = new Scanner(System.in);
      System.out.print("Enter a number: ");
      int num = scanner.nextInt();
      int factorial = 1;
      for (int i = 1; i <= num; i++) {
         factorial *= i;
      }
      System.out.println("The factorial of " + num + " is " + factorial);
   }
}